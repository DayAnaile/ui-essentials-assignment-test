const { req, res } = require('express');
const express = require('express');
const morgan = require('morgan');
const {Pool} = require('pg');

const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '../.env') })

let pool = new Pool();
const app = express();
const port = process.env.PORT;

app.use(morgan('dev'));
app.use(express.urlencoded({extended:true}));
app.use(express.json());

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/info/get', (req, res) => {
    try{
    pool.connect( async (error, client, release) => {
        let resp = await client.query('SELECT * FROM test');
        release();
        res.send(resp.rows);
    })
    }catch(error){
        console.log(error);
    }
});

app.post('/info/add', (req, res) => {
    try{
    pool.connect( async (error, client, release) => {
        let resp = await client.query(`INSERT INTO test(name) VALUES('${req.body.add}')`);
        res.redirect('/info/get');
    })
    }catch(error){
        console.log(error);
    }
});

app.post('/info/delete', (req, res) => {
    try{
    pool.connect( async (error, client, release) => {
        let resp = await client.query(`DELETE FROM test WHERE name = '${req.body.delete}'`);
        res.redirect('/info/get');
    })
    }catch(error){
        console.log(error);
    }
});

app.post('/info/update', (req, res) => {
    try{
    pool.connect( async (error, client, release) => {
        let resp = await client.query(`UPDATE test SET name = '${req.body.newValue}' WHERE name = '${req.body.oldValue}' `);
        res.redirect('/info/get');
    })
    }catch(error){
        console.log(error);
    }
});


app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`);
});